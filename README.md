# Lando - based Symfony development infrastructure.


Use Symfony on Lando for local development; powered by Docker and Docker Compose, config php version, swap db or caching backends or web server, use composer. symfony console, xdebug and custom config files, oh and also import and export databases.

## Local infrastructure ##


Local development infrastructure consists of:

```
- PHP 7.4
- Nginx
- MySQL
- phpMyAdmin
```

## Usage

First you need to install [Docker](https://docs.docker.com/engine/install/) and [Lando](https://docs.lando.dev/basics/installation.html#system-requirements)


After that you can create the project:

```
- cd some-dir
- git clone https://gitlab.com/Vitalii-Luka/lando-symfony.git
- cd lando-symfony
- lando start
```


## Tooling

By default each Lando Symfony recipe will also ship with helpful dev utilities.

This means you can use things like `console`, `composer` and `php` via Lando and avoid mucking up your actual computer trying to manage `php` versions and tooling.

[Lando Default Commands](https://docs.lando.dev/basics/usage.html#default-commands)

```bash
lando composer          Runs composer commands
lando console           Runs console commands
lando db-export [file]  Exports database from a service into a file
lando db-import <file>  Imports a dump file into database service
lando mysql             Drops into a MySQL shell on a database service
lando php               Runs php commands
```

### Usage examples

```bash
# Do a basic cache clear
lando console cache:clear

# Run composer install
lando composer install

# Drop into a mysql shell
lando mysql

# Check the app's php version
lando php -v
```
